package com.example.reboottest

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class MyBroadcastReciever: BroadcastReceiver() {
    override fun onReceive(p0: Context?, p1: Intent?) {
        if(p0!=null && p1!=null && p1.action == Intent.ACTION_BOOT_COMPLETED) {
            MyService.enqueueWork(p0, p1)
        }
    }
}